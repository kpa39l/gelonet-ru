# Устанавливаем и настравиваем node_exporter

## Описание

node-exporter написан на Golang и распространяется в виде исполняемого файла, включающего в себя все необходимы библиотеки.

## Установка

 Поэтому установка состоит из того чтобы скачать и скопировать файл с github проекта в папку включенную в PATH (то есть ту, в которой командная оболочка ищет использняемые файлы).

```# Скачиваем последнюю версию node_exporter со страницы проекта
curl -s https://api.github.com/repos/prometheus/node_exporter/releases/latest| grep browser_download_url|grep linux-amd64|cut -d '"' -f 4|wget -qi -
# Разархивируем полученный архив
tar -xvf node_exporter*.tar.gz
# Переходим в папку с исполняемым файлом
cd  node_exporter*/
# Копируем исполняемый файл в католог входящий в PATH для Debian это /usr/local/bin
sudo cp node_exporter /usr/local/bin
#Возвращаемся в домашний каталог
cd
```

Теперь, если выполнить в консоли сервера, на который мы скачали node_exporter, команду ``` node_exporter --version ```:

```$ node_exporter --version
node_exporter, version 1.6.1 (branch: HEAD, revision: 4a1b77600c1873a8233f3ffb55afcedbb63b8d84)
  build user:       root@586879db11e5
  build date:       20230717-12:10:52
  go version:       go1.20.6
  platform:         linux/amd64
  tags:             netgo osusergo static_build
```

## Настройка запуска

Первичная настройка node_exporter заключается в добавлении юнита systemd, чтобы он автоматически запускался при запуске/перезагрузке операционной системы, и в создании конфигурационного файла с параметрами запуска, чтобы каждый раз не лезть в файл юнита с настройками сервиса и не перечитывать каждый настройки systemd после его изменения. Со второго и начнём.

У node_exporter нет конфигурационного файлаб все настройки передаются через ключи командной строки. Но мы их вынесем в отдельный конфигурационный файл. В Debian файлы конфигураций размещаются в каталоге /etc/. В нём и создадим файл с пока еще пустым списком ключей запуска node_exporter.

```sudo cat <<EOF > /etc/node_exporter
OPTIONS=""
EOF
```

Для работы node_exporter нужны права root. Поэтому запускать мы его будем как системную службу. Файлы системных служб находятся в таких каталогах:

* /usr/lib/systemd/system

* /lib/systemd/system
* /etc/systemd/system
Причём файлы сервисов, поставляемые с пакетами, как правило, располагаются в каталогах /lib и /usr/lib, а в /etc/ расположены модифицированные файлы юнитов переопределяющие стандартные настройки.

```sudo cat <<EOF > /usr/lib/systemd/system/node_exporter.service
[Unit]
Description=Prometheus Node exporter for machine metrics
Documentation=https://github.com/prometheus/node_exporter

[Service]
Restart=always
User=root
EnvironmentFile=/etc/node_exporter
ExecStart=/usr/local/bin/node_exporter $OPTIONS
ExecReload=/bin/kill -HUP $MAINPID
TimeoutStopSec=20s
SendSIGKILL=no

[Install]
WantedBy=multi-user.target
EOF
```

С помощью директивы: EnvironmentFile все переменные из файла, указанного в этой директиве, добавляются в env, это позволяет нам передавать список ключей для Node Exporter через переменную $OPTIONS без изменения службы.

Обновляем список служб systemd, ведь он еще не знает о юните который мы создали раньше. Запускаем node_exporter и добавляем его в автозапуск.

```sudo systemctl daemon-reload
 sudo systemctl start node_exporter
 sudo systemctl enable node_exporter
```

Проверяем что node_exporter заработал.

```$ sudo systemctl status node_exporter
 node_exporter.service - Prometheus Node exporter for machine metrics
     Loaded: loaded (/etc/systemd/system/node_exporter.service; enabled; preset: enabled)
     Active: active (running) since Tue 2023-08-22 11:41:37 MSK; 51min ago
       Docs: https://github.com/prometheus/node_exporter
   Main PID: 78053 (node_exporter)
      Tasks: 6 (limit: 6951)
     Memory: 13.3M
        CPU: 317ms
     CGroup: /system.slice/node_exporter.service
             └─78053 /usr/local/bin/node_exporter 

авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=thermal_zone
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=time
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=timex
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=udp_queues
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=uname
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=vmstat
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=xfs
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=node_exporter.go:117 level=info collector=zfs
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=tls_config.go:274 level=info msg="Listening on" address=[::]:9100
авг 22 11:41:37 srvr-backup node_exporter[78053]: ts=2023-08-22T08:41:37.040Z caller=tls_config.go:277 level=info msg="TLS is disabled." http2=false address=[::]:9100
```

Как видно из вывода по умолчанию node_exporter работает на порту 9100. Проверим что он нам на нём выдаст

```$ curl -I http://localhost:9100
HTTP/1.1 200 OK
Content-Type: text/html; charset=UTF-8
Date: Tue, 22 Aug 2023 09:35:08 GMT
Content-Length: 961
```

![Страница экспозиции](page.png)

![Страница со значениями метрик](metrics.png)

## Настройка метрик
Теперь чтобы изменить настройки node_exporter нужно в созданный нами файл конфигурации /etc/node-exporter внутрб кавычек добавить нужный ключ и перезапустить сервис node_exporter. Например, если мы хотим изменить порт на котором проводится экспозиция собранных метрик и добавить сбор метрик из отдельного файла, то приводим файл настроек к виду:

```$ cat /etc/node_exporter
OPTIONS="--web.listen-address ":9100 --collector.textfile.directory=/tmp/node_exporter"
```

Ключ --collector.textfile.directory добавляет коллектор, который загружает все файлы *.prom из указаннаго каталога. Это файлы с метриками, создадам такой в указанном каталоге:

```mkdir /tmp/node_exporter/
cat <<EOF > /tmp/node_exporter/node_exporter_custom_metric.prom
#TYPE slurm_demo_metric counter
slurm_demo_metric 100
EOF
```

Часто используемые ключи:
**--log.level** Данный ключ устанавливает уровень логирования. Возможные уровни логирования: debug, info, warn, error. По-умолчанию: info

**--log.format** Данный ключ устанавливает формат логов. Доступные форматы: logfmt и json. По-умолчанию: logfmt

**--web.listen-address** Данный ключ устанавливает адрес и порт, по которому будет доступен Node Exporter. По-умолчанию: ":9100"

**--web.telemetry-path** Данный ключ устанавливает адрес, по которому доступны результаты экспозиции. По-умолчанию: "/metrics"

**--web.disable-exporter-metrics** Данный ключ устанавливает список метрик, которые будут исключены из экспозиции. По-умолчанию: Пустой. Например, для исключения всех метрик, имя которых начинается с go_, значение ключа будет: go_*. Допускается использовать перечисление нескольких метрик, с запятой в качестве разделителя.

Полный список ключей можно просмотреть с помощью ключа --help.

```node_exporter --help```

node_exporter идёт в комплекте с ножеством коллекторов (сборщиков) метрик. Список их довольно велик и большая часть из них по-умолчанию отключена. получить их список можно с помощью ключа --help.

```node_exporter --help```

## Использованные материалы

[Где находятся файлы сервисов Systemd](https://losst.pro/gde-nahodyatsya-fajly-servisov-systemd?ysclid=lllzuud6gp286727760)
[Install Prometheus and node_exporter on Debian 11|10](https://computingforgeeks.com/how-to-install-prometheus-and-node-exporter-on-debian/)
[Monitoring Linux host metrics with the Node Exporter](https://prometheus.io/docs/guides/node-exporter/)
